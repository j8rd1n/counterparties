﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NsiManager.Back.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using NsiManager.Back.DTO.CRM;
using NsiManager.Back.DTO.Enums;

namespace NsiManager.Back.DAL.Utils.CrmHelpers.Converters
{
    public class CounterpartiesCrmConverter
    {
        #region Поля

        private readonly Context Db;

        #endregion

        #region Конструктор

        public CounterpartiesCrmConverter(Context context)
        {
            Db = context;
        }

        #endregion

        #region Методы

        public Counterparties AccountsConvert(AccountsCallback accounts,
            Counterparties counterparties, CrmTypeEnum crmType)
        {
            if (CRMConvertHelper.IsDeleted(accounts.Deleted))
                return DeleteCounteparties(counterparties, crmType);

            UpdateCounterparties(accounts, counterparties);

            //Если источник - корп, то вызывается очистка только для общих контрагентов
            //в корп.контрагентах и так поля эти не заполнены
            if (crmType == CrmTypeEnum.Corp && counterparties.Mutual == 1)
                CRMConvertHelper.CleanMutualEntity(counterparties);

            return counterparties;
        }

        public async Task<Counterparties> CounterpartiesInit(AccountsCallback accounts, CrmTypeEnum crmType,
            List<RefEntity> refEntities, List<EntityType> entityTypes)
        {
            var counterparties = await GetCounterparties(accounts.Id, accounts.Contragent_id_c);

            if (!String.IsNullOrEmpty(accounts.Id) && !counterparties.SubjectId.HasValue)
                CounterpartiesSubjectInit(accounts, crmType, entityTypes, counterparties);            

            if (!counterparties.SubjectCompanyId.HasValue)
                counterparties.SubjectCompany =
                    await CreateCounterpartiesSubjectEntities(accounts, refEntities, entityTypes);

            counterparties.IsHoldingCounterparty ??= (int)crmType;

            return counterparties;
        }

        #endregion

        #region Служебные методы

        private static bool IsCommonEntity(Counterparties counterparties)
            => counterparties.IsHoldingCounterparty == (int)CrmTypeEnum.VstHolding
                || (counterparties.IsHoldingCounterparty == (int)CrmTypeEnum.Vst 
                        && counterparties.SubjectId.HasValue);

        private static void CounterpartiesSubjectInit(AccountsCallback accounts, CrmTypeEnum crmType,
            List<EntityType> entityTypes, Counterparties counterparties)
        {
            counterparties.Subject = CRMConvertHelper
                .CreateCrmSubject(accounts.Id, EntityTypeEnum.CRMType, entityTypes);
            if (crmType == CrmTypeEnum.Holding && counterparties.IsHoldingCounterparty == (int)CrmTypeEnum.Vst)
                counterparties.IsHoldingCounterparty = (int)CrmTypeEnum.VstHolding;
        }


        private static void UpdateCounterparties(AccountsCallback accounts, Counterparties counterparties)
        {
            CRMConvertHelper.FillCommonCounterpartiesFields(accounts, counterparties);

            if ((counterparties.IsHoldingCounterparty == 1 || counterparties.IsHoldingCounterparty == null)
                && counterparties.Mutual == 0)
                CRMConvertHelper.FillHoldingCounterpartiesFields(accounts, counterparties);
        }

        private static Counterparties DeleteCounteparties(Counterparties counterparties, CrmTypeEnum crmType)
        {
            var dateStamp = DateTime.Now;
            var isCommonEntity = IsCommonEntity(counterparties);
            var counterpartyCrmType = (CrmTypeEnum)counterparties.IsHoldingCounterparty;
            if (counterparties.Mutual == 0 && !isCommonEntity && counterpartyCrmType == crmType)
            {
                counterparties.DateDeleted = dateStamp;
                counterparties.SubjectCompany.SubjectCompanyNavigation.DateDeleted = dateStamp;
            }
            else
            {
                if (crmType != CrmTypeEnum.Vst)
                {
                    if (counterparties.Mutual == 0)
                    {
                        counterparties.Subject.DateDeleted = dateStamp;
                        counterparties.SubjectId = null;
                    }
                    counterparties.Mutual = 0;
                }

                counterparties.IsHoldingCounterparty = isCommonEntity
                    ? counterparties.IsHoldingCounterparty - (int)crmType
                    : crmType switch
                    {
                        CrmTypeEnum.Corp => 1,
                        CrmTypeEnum.Holding => 0,
                        _ => counterparties.IsHoldingCounterparty
                    };
            }

            return counterparties;
        }

        private async Task<Counterparties> GetCounterparties(string id, int crmId)
        {
            var counterparties = await Db.Counterparties
                .FirstOrDefaultAsync(x => x.ContragentId == crmId && x.DateDeleted == null);

            if (counterparties != null)
                return counterparties;

            var subject = await Db.Subject.FirstOrDefaultAsync(x => x.Code == id && x.DateDeleted == null);

            counterparties = await Db.Counterparties.FirstOrDefaultAsync(x =>
                x.SubjectId == subject.SubjectId
                && x.DateDeleted == null
                && x.ContragentId == crmId);

            return counterparties ?? new() { Subject = subject, SubjectId = subject?.SubjectId };
        }

        private async Task<SubjectCompany> CreateCounterpartiesSubjectEntities(AccountsCallback accounts,
            List<RefEntity> refEntities, List<EntityType> entityTypes)
        {
            var dateStamp = CRMConvertHelper.ToDateTimeConvert(accounts.Date_entered) ?? DateTime.Now;

            var clientTypeId = CRMConvertHelper.GetRefEntityIdByCode("client",
                EntityTypeEnum.CounterpartyType, refEntities, entityTypes);

            if (!clientTypeId.HasValue)
                throw new Exception($"Нет контрагента с типом client!");

            var subjectType = await Db.EntityType.FirstOrDefaultAsync(x => x.Code == "Company");
            var subjectTypeId = subjectType.EntityTypeId;
            var subjectCode = await Db.Subject.FirstOrDefaultAsync(x =>
                                  x.Code == accounts.Contragent_id_c.ToString()
                                  && x.SubjectTypeId == subjectTypeId
                                  && x.DateDeleted == null)
                              ?? await CreateSubjectCode(accounts, entityTypes, dateStamp);

            subjectCode.SubjectCompany ??= new()
            {
                SubjectCompanyId = subjectCode.SubjectId,
                CounterpartyTypeId = clientTypeId.Value,
                Inn = accounts.Tin_c,
                Kpp = accounts.Trrc_c,
                Psrn = accounts.Ogrn_c,
                DateCreated = dateStamp
            };

            if (subjectCode.SubjectId == 0)
                await Db.Subject.AddAsync(subjectCode);
            else
                Db.Subject.Update(subjectCode);

            await Db.SaveChangesAsync();

            return subjectCode.SubjectCompany;
        }

        private async Task<Subject> CreateSubjectCode(AccountsCallback accounts, List<EntityType> entityTypes, DateTime dateStamp)
        {
            var subjectCode = CRMConvertHelper.CreateCrmSubject(accounts.Contragent_id_c.ToString(),
                EntityTypeEnum.Company, entityTypes, dateStamp);

            var subjectBrand = await Db.Subject.FirstAsync(x => x.Code == CRMConvertHelper.BrandCode);
            subjectCode.SubjectLinkChild.Add(new SubjectLink()
            {
                ChildId = subjectCode.SubjectId,
                ParentId = subjectBrand.SubjectId,
                DateCreated = dateStamp
            });

            subjectCode.SubjectLang = new List<SubjectLang>
            {
                CRMConvertHelper.CreateSubjectLang(subjectCode.SubjectId, accounts.Name,
                                    accounts.Contragent_name_short_c, CRMConvertHelper.LangRuId, dateStamp),
                CRMConvertHelper.CreateSubjectLang(subjectCode.SubjectId, accounts.Name,
                        accounts.Contragent_name_short_c, CRMConvertHelper.LangEnId, dateStamp)
            };

            return subjectCode;
        }

        #endregion
    }
}
